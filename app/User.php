<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use DB;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    public $search_term = null;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email','phone_number','verified', 'social_login', 'password','verifyToken'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function block(){
        $this->blocked = 1;
        $this->save();
    }

    public function avatar()
    {
        return $this->hasOne('App\Avatar', 'user_id');
    }


    public function unblock(){
        $this->blocked = 0;
        $this->save();
    }

    public function preferences()
    {
        # code...
        return $this->hasMany('\App\Preference', 'user_id');
    }

    // public function products()
    // {
    //     # code...
    //            $preferences = $this->preferences;
    //     $products = [];
    //     if(count($preferences)){
    //         foreach($preferences as $preference){
    //             if(!count($preference->category->products)) continue;
    //            array_push($products ,$preference->category->products);
    //         }
    //     }

    //     return  $products;
    // }

    public function products()
    {
        return $this->hasManyThrough('App\Product', 'App\Preference', 'user_id','category_id', 'id', 'category_id');
    }

    public function searchProducts()
    {
        # code...
        $q = $this->search_term;
    return $this->hasManyThrough('App\Product', 'App\Preference', 'user_id','category_id', 'id', 'category_id')->distinct()
            ->where('description', 'like',"%$q%")
            ->orWhere('title', 'like',"%$q%");


    }

    public function search($q)
    {
        # code...
        $this->search_term = $q;
        return $this->searchProducts->unique();
    }

    public function remove()
    {
        \App\Avatar::where('user_id', $this->id)->delete();
        \App\Preference::where('user_id', $this->id)->delete();
        return $this->delete();
        # code...
    }
}
