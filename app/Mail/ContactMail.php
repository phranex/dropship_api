<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;
    public $mess;
    public $email;
    public $fullname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$mess, $fullname = null)
    {
        //
        $this->mess = $mess;
        $this->email = $email;
        $this->fullname = $fullname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->fullname ? $this->fullname : '';
        return $this->view('emails.contact-mail')->subject('Message from '.$user)
                                                ->replyTo($this->email);
    }
}
