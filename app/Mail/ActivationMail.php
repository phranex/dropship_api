<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$type = null)
    {
        //
        $this->user = $user;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        if($this->type == null)
            return $this->markdown('emails.user.activation')->subject('Activation Mail')->bcc('techplanet2018@gmail.com');
        else
            return $this->markdown('emails.user.reactivation')->subject('Activation Mail: Reminder')->bcc('techplanet2018@gmail.com');

    }
}
