<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCustomerAction extends Model
{
    //
    public function saveAction($product,$type,$user = null)
    {
        # code...
        $this->product_id = $product;
        $this->customer_action_type = strtolower($type);
        $this->user_id = $user;
        $this->save();

    }


    public static function getStat($type,$order)
    {
        # code...
        if($type == 'view' && $order == 'desc') $m = 'most_viewed';
        elseif($type == 'view' && $order == 'asc') $m = 'least_viewed';
        elseif($type == 'order' && $order == 'desc') $m = 'most_bought';
        elseif($type == 'order' && $order == 'asc') $m = 'least_bought';
        return self::where('customer_action_type',$type)->groupBy(['customer_action_type','product_id'])
        ->selectRaw('*,count(*) as '.$m.', product_id')
        ->orderBy($m,$order)
        ->first();
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
