<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    //

    public function createOrEdit($c)
    {
        # code...
        $this->user_id = auth()->id();
        $this->category_id = $c;
        $this->save();
    }

    public function category()
    {
        # code...
       return $this->belongsTo('\App\Category', 'category_id');
    }

    public static function stat()
    {
        # code...
        return self::orderBy('category_id','asc')
            ->selectRaw('*,count(*) as num_of_users,category_id')
            ->groupBy('category_id')->get();
    }
    public static function numOfUsers()
    {
        # code...
        return self::rightJoin('categories','categories.id','=','preferences.category_id')
        ->orderBy('category_id','asc')
    ->selectRaw('*, categories.id As id, preferences.id as preference_id, count(*) as num_of_users')
    ->groupBy('category_id')->get();
        // return self::orderBy('category_id','asc')
        //     ->selectRaw('count(*) as num_of_users,category_id')
        //     ->groupBy('category_id')->get();
    }
}
