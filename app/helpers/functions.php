<?php
    function apiResponse($status,$message,$data){
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }


    function getImageLink($link){
        return env('IMAGE_URL').'/storage/'.$link;
    }

    function sendInvitation($contact,$message){
        //send invitation to email
        if(isset($contact->fullname)){
            //
        }
    }

    function uploadFile($request,$location,$file_prefix)
    {
        # code...

        if($request){
            $file =$request ;

            $directory = $location;
           $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file
           $fileName = $file_prefix.'_'.$name.'.'.$file->extension();
           $path = $file->storeAs($directory,$fileName);
            return $path;
        }

        return null;
    }

    function currency_format($amt){
        echo '$'.number_format($amt);
    }

    function when($string){
        echo \Carbon\Carbon::parse($string)->diffForHumans();
    }


?>
