<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function createOrEdit($cat, $path = null)
    {
        # code...
        $this->description = request('description')?request('description'):null;
        $this->path = $path;
        // $this->cloud_id = $cloud_id;
        $this->name = $cat;
        $this->save();
    }
    public function products()
    {
        # code...
        return $this->hasMany('\App\Product', 'category_id');
    }

    public function preferences()
    {
        # code...
        return $this->hasMany('\App\Preference', 'category_id');
    }



    public static function numOfUsers()
    {
        # code...
        return self::leftJoin('preferences','categories.id','=','preferences.category_id')
                ->orderBy('category_id','asc')
            ->selectRaw('*, categories.id As id, preferences.id as preference_id, count(user_id) as num_of_users')
            ->groupBy('category_id')->get();
    }

    public function remove()
    {
        # code...
        \App\Product::where('category_id', $this->id)->update(['category_id' => '-1']);
        \App\Preference::where('category_id', $this->id)->delete();
        return $this->delete();
    }
}
