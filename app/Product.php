<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
class Product extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function createOrEdit($request)
    {
        # code...
        $this->title = $request->title;
        $this->price = $request->price;
        $this->retail_price = $request->retail_price;
        $this->affiliate_link = $request->affiliate_link;
        $this->description = $request->description;
        $this->status = $request->status? 1 : 0; // 1= approved, 0 = rejected
        $this->visibility = $request->visibility? 1: 0;
        $this->slug = str_slug($this->title, '_');
        $this->category_id = $request->category_id;
        return $this->save();

    }

    public function generateRedirectCode()
    {
        # code...
        $this->redirect_code = base64_encode($this->title.'_'.$this->id);
        return $this->save();
    }
    public function images()
    {
        # code...
        return $this->hasMany('\App\ProductImage','product_id');
    }

    public function removeImages()
    {
        # code...
        if(count($this->images)){
            $images = $this->images;
            foreach($images as $image){
                Storage::delete($image->path);
            }
        }
        \App\ProductImage::where('product_id',$this->id)->delete();

    }

    public function remove()
    {
        # code...
        $this->removeImages();
        $this->delete();
    }

    public function customerActions()
    {
        # code...
        return $this->hasMany('\App\ProductCustomerAction', 'product_id');
    }

    public function views()
    {
        # code...
        return count($this->hasMany('\App\ProductCustomerAction', 'product_id')->where('customer_action_type','view')->get());
    }

    public function affiliate_clicks()
    {
        # code...
        return count($this->hasMany('\App\ProductCustomerAction', 'product_id')->where('customer_action_type','order')->get());
    }

    public static function getStat($type,$order)
    {
        # code...
        if($type == 'view' && $order == 'desc') $m = 'most_viewed';
        elseif($type == 'view' && $order == 'asc') $m = 'least_viewed';
        elseif($type == 'order' && $order == 'desc') $m = 'most_bought';
        elseif($type == 'order' && $order == 'asc') $m = 'least_bought';
        return self::leftJoin('product_customer_actions','products.id','product_customer_actions.product_id')
                ->where('customer_action_type',$type)
                ->groupBy(['customer_action_type','product_id'])
                ->selectRaw('*,products.id AS id, product_customer_actions.id AS product_customer_action_id,count(*) as '.$m.', product_id')
                ->orderBy($m,$order)
                ->first();
    }
}
