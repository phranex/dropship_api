<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    //
    public function createOrEdit($full, $half)
    {
        # code...
        $this->user_id = auth()->id();
        $this->full_path = $full;
        $this->relative_path = $half;
        return $this->save();
    }
}
