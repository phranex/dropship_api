<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    //
    public function make($request, $type)
    {
        $this->visitor_tracker_id = isset($request->visitor_tracker_id) ? $request->visitor_tracker_id : null;
        $this->url = $request->url;
        $this->previous_url = isset($request->previous_url) ? $request->previous_url: null;
        $this->page_name = $request->page_name;
        $this->type = $type;
        return $this->save();

    }
}
