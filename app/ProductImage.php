<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    public function createImage($id,$path)
    {
        # code...
        $this->product_id = $id;
        $this->path = $path;
        $this->save();
    }
}
