<?php

namespace App\Http\Controllers;

use App\Visit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\ProductCustomerAction;
use App\Product;
use App\Preference;
use App\Category;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //users statistics
        // total number of users
        $total_users = User::all()->count();
        //total number of unverified users
        $total_verified_users = User::where('verified',1)->count();
        //total number of verified users
        $total_unverified_users = User::where('verified',0)->count();
        //total number of blocked users\
        $total_blocked_users = User::where('blocked',1)->count();

        $total_num_of_todays_users = count(User::whereDate('created_at', \Carbon\Carbon::now()->format('Y-m-d'))->get());
        // $from = \Carbon\Carbon::now()->subWeek()->startOfWeek();
        $last_week = \Carbon\Carbon::now()->startOfWeek();
        $last_month = \Carbon\Carbon::now()->startOfMonth();

        $now = \Carbon\Carbon::now();
        $total_num_of_last_weeks_users = count(User::whereBetween('created_at', [$last_week, $now])->get());
        $total_num_of_last_months_users = count(User::whereBetween('created_at', [$last_month, $now])->get());

        $last_month = strtotime($last_month);
        $last_week = strtotime($last_week);
        //products statistics

        //most viewed(
       $most_viewed = Product::getStat('view','desc');
    //    $most_viewed_product = $most_viewed->product;
        //most bought
        $most_bought = Product::getStat('order','desc');
        // $most_bought_product = $most_bought->product;
        //least viewed
        $least_viewed = Product::getStat('view','asc');
        // $least_viewed_product = $least_viewed->product;
        //least bought
        $least_bought = Product::getStat('order','asc');
        // $least_bought_product = $least_bought->product;

        //category statistics

        $cat_stats = Category::numOfUsers();


        //best selling categories

        //least selling category

        //category user stat

        //visits stats
        if(empty(request('from')) && empty(request('to'))){
            $total_visits = Visit::latest()->groupBy('visitor_tracker_id')->get()->count();
            $total_new_visitors = Visit::whereDate('created_at', Carbon::now())->where('type', 'new')->get()->count();
            $total_repeat_visitors = Visit::whereDate('created_at', Carbon::now())
                ->where('type', 'repeat')
                ->groupBy('visitor_tracker_id')
                ->get()
                ->count();
            $total_logins = Visit::where('page_name', 'dashboard')
                ->where('previous_url',env('FRONTEND_URL').'/login')
                ->where('url', env('FRONTEND_URL').'/user/products')
                ->whereDate('created_at', Carbon::now())
                ->groupBy('visitor_tracker_id')
                ->get()
                ->count();
        }else{
            $from_date = Carbon::parse(request('from'));
            $to_date = Carbon::parse(request('to'));
            $total_visits = Visit::whereBetween(DB::raw('DATE(created_at)'), array($from_date, $to_date))
                                ->latest()->groupBy('visitor_tracker_id')->get()->count();
            $total_new_visitors = Visit::whereBetween(DB::raw('DATE(created_at)'), array($from_date, $to_date))
                                        ->where('type', 'new')->get()->count();
            $total_repeat_visitors = Visit::whereBetween(DB::raw('DATE(created_at)'), array($from_date, $to_date))
                ->where('type', 'repeat')
                ->groupBy('visitor_tracker_id')
                ->get()
                ->count();
            $total_logins = Visit::where('page_name', 'dashboard')
                ->where('previous_url',env('FRONTEND_URL').'/login')
                ->where('url', env('FRONTEND_URL').'/user/products')
                ->whereBetween(DB::raw('DATE(created_at)'), array($from_date, $to_date))
                ->groupBy('visitor_tracker_id')
                ->get()
                ->count();
        }





        return view('admin.dashboard',compact(
            'cat_stats',
            'total_visits',
            'total_new_visitors',
            'total_logins',
            'total_repeat_visitors',
            'total_users',
            'most_bought',
            'total_unverified_users',
            'total_verified_users',
            'total_blocked_users',
            'total_num_of_todays_users',
            'total_num_of_last_weeks_users',
            'total_num_of_last_months_users',
            'last_week',
            'last_month',
            'now',
            'most_viewed',
            'least_bought',
            'least_viewed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
