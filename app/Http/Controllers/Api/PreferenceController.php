<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Preference;

class PreferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $categories = request('preferences');

        if(gettype($categories) == 'array'){
            if(count($categories)){
                //check user preference
                $user_preferences = auth()->user()->preferences->pluck('category_id')->toArray();
                if(count($user_preferences)){
                    $cat = array_diff($categories,$user_preferences);
                }else{
                    $cat = $categories;
                }

                if(count($cat)){
                    foreach ($cat as $c) {
                        # code...
                        $preference = new Preference;
                        $preference->createOrEdit($c);
                    }
                }
                return apiResponse(1,trans('Success'),[]);

            }
            return apiResponse(0, trans('No preference sent'), [] );
        }

        return apiResponse(0,trans('Invalid data sent'), [request()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(is_numeric($id)){
            $preference = Preference::find($id);
            if($preference){
                if($preference->user_id == auth()->id()){
                    $preference->delete();
                    return apiResponse(1,trans('Deleted'), []);
                }
            }

        }
        return apiResponse(0,trans('Invalid request'), []);
    }
}
