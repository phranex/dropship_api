<?php

namespace App\Http\Controllers\Api;

use App\Visit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitController extends Controller
{
    //
    const TRACKER_CODE = 'CONSU';
    public function getTrackerId(Request $request){
        if(empty(request('url')) || empty(request('page_name')))
            return apiResponse(0,'error', []);

        $tracker_id = $this->generateTrackerCode();
//        do{
//            $flag = true;
//            $tracker_id = $this->generateTrackerCode();
//            $v = Visit::where('visitor_tracker_id',$tracker_id)->first();
//            if(empty($v)){
//                $flag = false;
//            }
//        }while ($flag == true);
        $request->visitor_tracker_id = $tracker_id;
        $visit = new Visit;
        $visit->make($request, 'new');
        return apiResponse(1,'success', ['tracker_id' => $tracker_id] );
    }

    public function store(Request $request, Visit $visit){
        if(empty(request('url')) || empty(request('visitor_tracker_id')) || empty(request('page_name')))
            return apiResponse(0,'error', []);
        $tracker_id = request('visitor_tracker_id');
        $v = Visit::where('visitor_tracker_id',$tracker_id)->first();
        if(!empty($v)){
            $visit->make($request, 'repeat');
            return apiResponse(1,'success', [] );

        }
        return $this->getTrackerId($request);
    }

    private function generateTrackerCode()
    {
        $rand = mt_rand(100000, 999999) * rand(1, 100);
        return self::TRACKER_CODE.'_'.$rand.time();
    }
}
