<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Http\Resources\ProductResource;
use App\ProductCustomerAction;

class ProductController extends Controller
{
    //

    public function show($id)
    {
        # code...
        if(empty($id)) return apiResponse(0, trans('No data sent'), []);
        if(is_numeric($id)){
            $product = Product::find($id);
            if($product){

                $orderAction = new ProductCustomerAction;
                if(auth()->check()) $user = auth()->id(); else $user = null;
                $orderAction->saveAction($product->id,'view',$user);
                $product = new ProductResource($product);

                return apiResponse(1, trans('success'), $product);
            }
            else
                return apiResponse(0, trans('Product not found'), []);
        }

        return apiResponse(0, trans('Invalid data sent'), []);
    }


    public function getSimilarProducts($id)
    {
        # code...
        if(empty($id)) return apiResponse(0, trans('No data sent'), []);
        if(is_numeric($id)){
            $product = Product::find($id);

            if($product){
                $category_id = $product->category_id;
                $products = Product::where('category_id',$category_id)->where('id','!=',$product->id)->latest()->take(5)->get();

                $products = ProductResource::collection($products);

                return apiResponse(1, trans('success'), $products);
            }
            else
                return apiResponse(0, trans('Product not found'), []);
        }

        return apiResponse(0, trans('Invalid data sent'), []);
    }

    public function getAffiliateLink($code)
    {
        # code...
        $product = Product::where('redirect_code',$code)->first();
        if($product){
            //perform some operations to monitor how many times it has been ordered.
            $orderAction = new ProductCustomerAction;
            if(auth()->check()) $user = auth()->id(); else $user = null;
            $orderAction->saveAction($product->id,'order',$user);
            return apiResponse(1,trans('success'),['affiliate_link' => $product->affiliate_link]);
        }
        return apiResponse(0,trans('Product not found'),[]);
    }
}
