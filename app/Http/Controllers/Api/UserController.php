<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductCollection;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
//
class UserController extends Controller
{
    //
    public function getProductsByPreference()
    {
        try{
            $products = User::find(auth()->id())->products()->paginate(20);
            $total_price = auth()->user()->products->pluck('price')->sum();
            $total_retail_price = auth()->user()->products->pluck('retail_price')->sum();
            return $products = new ProductCollection($products, $total_price, $total_retail_price);

        }catch(\Exception $e){
            return apiResponse(0, trans('error'), []);
        }

        # code...
    }

    public function getProductsByCategory($category)
    {       if(!empty($category) && is_numeric($category)){
                $products = ProductResource::collection(auth()->user()->products->where('category_id',$category));
                $total_price = auth()->user()->products->pluck('price')->sum();
                $total_retail_price = auth()->user()->products->pluck('retail_price')->sum();
                $result = [
                    'products' => $products,
                    'total_price' => $total_price,
                    'total_retail_price' => $total_retail_price
                ];
                // logger($products);
                return apiResponse(1,trans('Success'), $result);


            }
            return apiResponse(0,trans('Please check category'), []);


        # code...
    }

    public function searchPreferenceProducts()
    {
        $form = request()->all();
        if(isset($form['query'])){
            $q = $form['query'];

            $products = auth()->user()->search($q);


            if($products){
                $products = ProductResource::collection($products);
                return apiResponse(1,trans('Success'), $products);


            }
            return apiResponse(1,trans('No result found'), []);
        }

            return apiResponse(0,trans('Please check category'), []);


        # code...
    }

    public function uploadAvatar()
    {
        # code...

        if(request()->hasFile('avatar')){

            //check if user already has  an avatar
            $avatar = \App\Avatar::where('user_id', auth()->id())->first();
            if(!isset($avatar)){
                // return "true";
                $avatar = new \App\Avatar;
            }
            else{

                Storage::delete($avatar->relative_path);

            }
            $file  =request()->file('avatar');

            $directory = 'site-uploads/users_avatar';
            // $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file
            $name = strtolower(auth()->user()->first_name.'-'.auth()->user()->last_name);
            $fileName = $name.time().'.'.$file->extension();
            $half = $file->storeAs($directory,$fileName);
            $full = env('IMAGE_URL').'/storage/'.$half;

            if($avatar->createOrEdit($full,$half)){
                return apiResponse(1,'Avatar update', ['full_path'=> $full, 'relative_path' => $half]);
            }
            apiResponse(0,'An unexpected error occurred', []);
        }

        return apiResponse(0,'No file was sent', []);

    }
}

