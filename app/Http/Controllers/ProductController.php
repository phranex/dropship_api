<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.*/
    public function index()
    {
        //
        $products = Product::paginate(20);
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = \App\Category::all();
        return view('product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd(request()->all());
        $this->validate($request, [
            'title' => 'required',
            'price' => 'required|numeric',
            'affiliate_link' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'photo[].*' => 'required|image|array|max:2000'
        ]);


        $product = Product::where('title', $request->title)->where('affiliate_link',$request->affiliate_link)->first();
        if(!$product){
            $product = new Product;
            $request_data = request()->all();
            if($product->createOrEdit($request)){
                $code = $product->generateRedirectCode();
                $num = count(request()->file('photo'));
                for($i = 0; $i < $num; $i++){
                    $image = request()->file('photo')[$i];

                    if($path = uploadFile($image,'uploads/products_images','product')){
                        $productImage = new \App\ProductImage;
                        $productImage->createImage($product->id,$path);
                    }
                }


                return redirect()->route('admin.products')->with('success',trans('Product added successfully'));
            }
            return back()->with('error', trans('Product couldn\'t be added'));
        }
        return back()->with('error', trans('Product already exist'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(is_numeric($id)){
            $product = Product::find($id);
            if($product){

                return view('product.show', compact('product'));
            }
            return back()->with('error', trans('Product Not found'));
        }
        return back()->with('error', trans('An error occurred'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(is_numeric($id)){
            $product = Product::find($id);
            if($product){
                $categories = \App\Category::all();
                return view('product.edit', compact('product','categories'));
            }
            return back()->with('error', trans('Product Not found'));
        }
        return back()->with('error', trans('An error occurred'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //


        $this->validate($request, [
            'title' => 'required',
            'price' => 'required|numeric',
            'affiliate_link' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'photo[].*' => 'required|image|array|max:2000'
        ]);
        $product = Product::find($id);

        if($product){

            $request_data = request()->all();
            if($product->createOrEdit($request)){
                // $code = $product->generateRedirectCode();
                // $product->removeImages();

                if(!empty(request('image_id'))){
                    $num_of_images_left = count(request('image_id'));
                    $images_left_ids = request('image_id');
                    if($num_of_images_left){
                        $image_to_remove = $product->images->whereNotIn('id',$images_left_ids);
                        // return $image_to_remove;
                        if($image_to_remove){
                            foreach($image_to_remove as $img){
                                Storage::delete($img->path);
                                $img->delete();

                            }
                        }
                    }
                }
                if(!empty(request()->file('photo'))){
                   $num = count(request()->file('photo'));
                    foreach (request()->file('photo') as $file){
                        $image = $file;
                        if($path = uploadFile($image,'upload/products_images','product')){
                            $productImage = new \App\ProductImage;
                            $productImage->createImage($product->id,$path);
                        }
                    }


                }




                return redirect()->route('admin.product.show', $product->id)->with('success',trans('Product updated successfully'));
            }
            return back()->with('error', trans('Product couldn\'t be added'));
        }
        return back()->with('error', trans('Product does not exist'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(is_numeric($id)){
            $product = Product::find($id);
            if($product){
                $product->remove();
                return redirect(route('admin.products'))->with('success',trans('Product deleted successfully'));
            }
        }
        return back()->with('error',trans('An error occurred'));
    }
    public function banner($product_id, $status)
    {
        # code...
        if(is_numeric($product_id) && is_numeric($status)){
            $product = Product::find($product_id);
            if($product){
                $product->show_on_banner = $status;
                $product->save();
                return back()->with('success',trans('Product Status Changed successfully'));
            }
        }
        return back()->with('error',trans('An error occurred'));
    }
}
