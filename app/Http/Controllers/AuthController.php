<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JWT;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use App\Mail\ActivationMail;
use Illuminate\Http\Request;
use App\User;
use Mail;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller
{
    //
       /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        logger($credentials);

        if (! $token = auth()->attempt($credentials)) {
            return apiResponse(0, 'Invalid Credentials', []);
            // return response()->json(['error' => 'Invalid Credentials'], 411);
        }

        return $this->respondWithToken($token);
    }

    public function  register(){
        $user = User::where(['email' => request('email')])->first();
        if(!empty(request('social'))){
            logger($user);
            if(!empty($user)){
                return $this->login();
            }
        }

        if(empty($user)){
            //validate request
            $form_request = request()->all();

            //return  response()->json(['status' => 0, 'message' => 'An unexpected error occurred','data' => $form_request]);

            if($this->validate_request($form_request)){
                $pass = $form_request['password'];
                $user = User::create([
                    'first_name' => $form_request['first_name'],
                    'last_name' => $form_request['last_name'],
                    'email' => $form_request['email'],
                    'phone_number' => isset($form_request['phone_number']) ? $form_request['phone_number'] : null,
                    'social_login' => empty(request('social')) ? null : 1,
                    'verifyToken' => \Illuminate\Support\Str::random(40),
                    'password' => Hash::make($pass),
                ]);
                logger($user);

                if($user){
                    //send email to user
                    $this->sendVerificationEmail($user);
                    return $this->login();
                }else{
                  return  apiResponse(0,__('An unexpected error occurred'), []);
                }
            }else{
                return  apiResponse(0,__('An error occurred. Please ensure that all fields are filled correctly'), []);
            }

        }

        // if(request('social')){
        //     return $this->login();
        // }

        return apiResponse(0,__('Email already exists'), []);


    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user();
        $userResource = new UserResource($user);
        if($userResource)
        return apiResponse(1,'success', $userResource);

        return apiResponse(0,'error', []);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'status' => 1,

            'message' => 'Login Successful',
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'user' => new UserResource(auth()->user()),
                // 'business_name' => @auth()->user()->setting->business_name,
                'expires_in' => auth()->factory()->getTTL() * 60
            ],

        ]);
    }

    public function validate_request($post_request)
    {
        # code...
        if(count($post_request)){
            foreach($post_request as $item){
                if(!isset($item))
                    return false;
            }
            return true;
        }
        return false;

    }

    public function changePassword(Request $request)
    {
        if(auth()->user()->blocked)
        return apiResponse(0, 'Blocked', []);

         $form_request = request()->all();
         if($this->validate_request($form_request)){
            $old = $form_request['old_password'];
            $user = \App\User::find(auth()->id());
            if($user){
                if(Hash::check($old,$user->password)){
                    $user->password = Hash::make($form_request['password']);
                    $user->save();
                    auth()->logout();
                    return apiResponse(1, 'success', []);
                }else{
                    return apiResponse(56, trans('Your password is incorrect'), []);
                    // return back()->with('error', 'Your password is incorrect');
                }
            }
            return apiResponse(0, trans('An enexpected error occurred'), []);

         }


    }

    public function editProfile()
    {
        # code...
        $form_request = request()->all();
        if(!empty($form_request['first_name']) && !empty($form_request['last_name'])){
            $user = \App\User::find(auth()->id());
            $user->first_name = $form_request['first_name'];
            $user->last_name = $form_request['last_name'];
            $user->phone_number = $form_request['phone_number'];
            $user->save();
            return apiResponse(1, trans('success'), []);
        }else{
            return apiResponse(0, trans('Please fill all details correctly'), []);
        }

        return apiResponse(0, trans('An enexpected error occurred'), []);
    }

    public function sendVerificationEmail($user, $type= null){
        // $when = \Carbon\Carbon::now()->addSeconds(5);
            try{
                Mail::to($user)->send(new ActivationMail($user, $type));
                return true;

            }catch(Exception $e){
                return  session(['error' => 'Verification mail could not be sent']);
            }

            return false;

    }


    public function verify()
    {
        # code...
        $form_request = request()->all();
        if(!empty($form_request['email']) && !empty($form_request['token'])){
            $email = $form_request['email'];
            $token = $form_request['token'];
            $user = User::where(['email' => $email, 'verifyToken' => $token])->first();
            if($user){
                $status = $user->update([
                    'verified' => '1',
                    'verifyToken'=> null
                ]);

                if($status){

                    if (! $token = auth()->login($user) ) {
                        return apiResponse(0, 'Invalid Credentials', []);
                        // return response()->json(['error' => 'Invalid Credentials'], 411);
                    }

                    return $this->respondWithToken($token);
                }
            }
            return apiResponse(56,'User not found', []);
        }
        return apiResponse(0,'Missing parameters sent', []);
    }


    public function resendMail()
    {
        # code...
        $user = auth()->user();
        $user->verifyToken = \Illuminate\Support\Str::random(40);
        $user->save();
        if($this->sendVerificationEmail($user,'resend')){
            return apiResponse(1,'success', []);
        }
        return apiResponse(0,'An error occurred trying to send a mail', []);

    }


}
