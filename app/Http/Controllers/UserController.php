<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::latest()->paginate(20);
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(is_numeric($id)){
            $user = User::find($id);


            if($user){
                return view('users.show', compact('user'));
            }
            return back()->with('error', trans('User Not found'));
        }
        return back()->with('error', trans('An error occurred'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(is_numeric($id)){
            $user = User::find($id);
            if($user){
                $user->remove();
                return back()->with('success', trans('User has been excluded'));
            }
            return back()->with('user', 'User does not exist');
        }
        return back()->with('error', 'Invalid Operation');
    }

    public function block($id)
    {
        //
        $user = User::find($id);
        if($user){
            $user->block();
            return back()->with('success', trans('User has been restricted'));


        }

        return back()->with('error', trans('An unexpected error occurred'));
    }
    public function unblock($id)
    {
        //
        $user = User::find($id);
        if($user){
            $user->unblock();
            return back()->with('success', trans('User has given full access '));


        }

        return back()->with('error', trans('An unexpected error occurred'));
    }
    public function uploadAvatar()
    {
        # code...

        if(request()->hasFile('avatar')){

            //check if user already has  an avatar

            $avatar = \App\Avatar::where('user_id', auth()->id())->first();
            if(!isset($avatar)){
                // return "true";
                $avatar = new \App\Avatar;
            }
            else{

                Storage::delete($avatar->relative_path);

            }
            $file  =request()->file('avatar');

            $directory = 'site-uploads/users_avatar';
            // $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file
            $name = strtolower(auth()->user()->first_name.'-'.auth()->user()->last_name);
            $fileName = $name.time().'.'.$file->extension();
            $half = $file->storeAs($directory,$fileName);
            $full = env('IMAGE_URL').'/storage/'.$half;

            if($avatar->createOrEdit($full,$half)){
                return apiResponse(1,'Avatar update', ['full_path'=> $full, 'relative_path' => $half]);
            }
            apiResponse(0,'An unexpected error occurred', []);
        }

        return apiResponse(0,'No file was sent', []);

    }

    public function excel()
    {
        # code...
        $users = User::all();


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'First Name');
        $sheet->setCellValue('B1', 'Last Name');
        $sheet->setCellValue('C1', 'Email');
        $sheet->setCellValue('D1', 'Phone Number');
        $sheet->setCellValue('E1', 'Date of Registration');
        $sheet->setCellValue('F1', 'Preferences');
        $row = 2;
        foreach($users as $user){
            $sheet->setCellValue('A'.$row, $user->first_name);
            $sheet->setCellValue('B'.$row, $user->last_name);
            $sheet->setCellValue('C'.$row, $user->email);
            $sheet->setCellValue('D'.$row, $user->phone_number);
            $sheet->setCellValue('E'.$row, Carbon::parse($user->created_at)->format('d-M-Y'));
            $preferences = $user->preferences;
            $cat = [];
            if(count($preferences)){
                foreach($preferences as $preference){
                    array_push($cat, ucwords($preference->category->name));
                }
            }
            $sheet->setCellValue('F'.$row, implode(',',$cat));
            $row++;
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-disposition: attachment; filename='.config('app.name').'-users.xlsx');
        // $writer = new Xlsx($spreadsheet);
        $writer = IOFactory::createWriter($spreadsheet,'Xlsx');
        $writer->save('php://output');
    }

    public function csv()
    {
        # code...
        $users = User::all();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $row = 1;
        foreach($users as $user){
            $sheet->setCellValue('A'.$row, $user->first_name);
            $sheet->setCellValue('B'.$row, $user->last_name);
            $sheet->setCellValue('C'.$row, $user->email);
            $sheet->setCellValue('D'.$row, $user->phone_number);
            $sheet->setCellValue('E'.$row, Carbon::parse($user->created_at)->format('d-M-Y'));
            $preferences = $user->preferences;
            $cat = [];
            if(count($preferences)){
                foreach($preferences as $preference){
                    array_push($cat, ucwords($preference->category->name));
                }
            }
            $sheet->setCellValue('F'.$row, implode('|',$cat));


            $row++;
        }

        header("Content-type: text/csv");
        header('Content-Disposition: attachment; filename='.config('app.name').'-subscription-emails.csv');
        header("Pragma: no-cache");
        // $writer = new Xlsx($spreadsheet);
        $writer = IOFactory::createWriter($spreadsheet,'Csv');
        $writer->save('php://output');
    }
}
