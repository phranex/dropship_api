<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subscriptions = \App\Subscription::paginate(20);
        return view('subscription.index', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        //
    }

    public function excel()
    {
        # code...
        $subscriptions = Subscription::all();


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Email');

        $row = 2;
        foreach($subscriptions as $subscription){

            $sheet->setCellValue('A'.$row, $subscription->email);

            $row++;
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-disposition: attachment; filename='.config('app.name').'-subscription-emails.xlsx');
        // $writer = new Xlsx($spreadsheet);
        $writer = IOFactory::createWriter($spreadsheet,'Xlsx');
        $writer->save('php://output');
    }

    public function csv()
    {
        # code...
        $subscriptions = Subscription::all();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $row = 1;
        foreach($subscriptions as $subscription){

            $sheet->setCellValue('A'.$row, $subscription->email);



            $row++;
        }

        header("Content-type: text/csv");
        header('Content-Disposition: attachment; filename='.config('app.name').'-subscription-emails.csv');
        header("Pragma: no-cache");
        // $writer = new Xlsx($spreadsheet);
        $writer = IOFactory::createWriter($spreadsheet,'Csv');
        $writer->save('php://output');
    }
}
