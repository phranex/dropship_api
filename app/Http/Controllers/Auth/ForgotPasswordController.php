<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        // $this->validateEmail($request);
        // logger($request->only('email'));
        if(!empty(request('email'))){
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );



            if($response == Password::RESET_LINK_SENT)
                return apiResponse(1,'success',[]);
            else if($response == Password::INVALID_USER)
                return apiResponse(56,'error',[]);
            return apiResponse(0,'error',[]);
        }
        return apiResponse(0,'No email was sent',[]);



        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.

    }
}
