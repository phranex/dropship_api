<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use Illuminate\Support\Facades\Storage;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::all();
        return view('category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $this->validate($request,[
            'category' => 'required',
        ]);
        $categories = explode(',', request('category'));
        if(empty(request('id'))){
            if(count($categories) > 1){
                $categories = array_map('trim', $categories);
                foreach($categories as $cat){
                    //check if name already exists
                    $catAlreadyExists = Category::where('name', $cat)->first();
                    if(!$catAlreadyExists){
                        $category = new Category;
                        $category->createOrEdit($cat);
                    }
                }
                return back()->with('success', 'Created categories succesfully');
            }else{
                $this->validate($request,[
                    'category' => 'required',
                    'description' => 'required|min:10',
                    "category_image"    => "required|min:1",
                ]);

                if(request()->hasFile('category_image')){
                    $file = request()->file('category_image');
                    $directory = 'uploads/categories';
                   $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file
                   $fileName = "category_".$name.'.'.$file->extension();
                   $path = $file->storeAs($directory,$fileName);
                        $cat = request('category');
                        $category = new Category;
                        $category->createOrEdit($cat, $path);
                        return back()->with('success', trans('Added Successfully'));
                }

            }
        }else{

        }



        return back()->with('error', trans('Unexpected error occurred'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

        $id = request('id');
        $category = Category::find($id);
        $path = $category->path;

        if($category){
            $rule = "";
            if(request('changeImage')){
                if(request()->hasFile('editImage'))
                    $rule =  "required|min:1";
                else
                    return back()->with('error', 'You clicked to change the image. Please upload one to continue');
            }

            $this->validate($request,[
                'editCategory' => 'required',
                // 'description' => 'required|min:10',
                'id' => 'required',
                'editImage' => $rule
            ]);

            if(request()->hasFile('editImage')){
                $file = request()->file('editImage');
                $directory = 'uploads/categories';
               $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);// get name of file
               $fileName = "category_".$name.'.'.$file->extension();
               //delete previous
               Storage::delete($category->path);
               $path = $file->storeAs($directory,$fileName);

            }
            $cat = request('editCategory');

            $category->createOrEdit($cat, $path);
            return back()->with('success', trans('Modified Successfully'));
            // dd(request()->all());
        }

        return back()->with('error',trans('An unexpected error occurred'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);
        if($category){
            if($category->remove()){
                 //activate cloudinary

                return back()->with('success', 'Deleted Successfully');
            }
        }
        return back()->with('error', 'Couldnt delete. Try again');
    }

}
