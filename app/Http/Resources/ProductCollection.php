<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    private $others;
    private $total_price;
    private $total_retail_price;

    public function __construct($resource, $total_price = null, $total_retail_price = null) {
        // Ensure we call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;
        $this->total_price = $total_price; // $apple param passed
        $this->total_retail_price = $total_retail_price;
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this;

        return [
            'status' => 1,
            'message' => 'success',
            'data' => [
                'products' => $this->collection->transform(function ($data){
                    return ['id' => $data->id,
                        'title' => $data->title,
                        'price' => $data->price,
                        'retail_price' => $data->retail_price,
                        'description' => $data->description,
                        'affiliate_link' => $data->affiliate_link,
                        'redirect_code' => $data->redirect_code,
                        'slug' => $data->slug,
                        'images' => ProductImageResource::collection($data->images)
                    ];
                }),
                'total_price' => $this->total_price,
                'total_retail_price' => $this->total_retail_price

            ],

        ]  ;
//        return parent::toArray($request);
    }
}
