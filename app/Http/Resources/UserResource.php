<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PreferenceResource;


class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return  [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'fullname' => $this->first_name.' '.$this->last_name,
            'email' => $this->email,
            'phone_number' => @$this->phone_number,
            'blocked' => $this->blocked,
            'verified' => $this->verified,
            'registered' => $this->created_at,
            'avatar' => @$this->avatar->full_path,
            'preferences' => PreferenceResource::collection($this->preferences),
        ];
    }
}
