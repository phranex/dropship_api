<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductImageResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
            'retail_price' => $this->retail_price,
            'description' => $this->description,
            'affiliate_link' => $this->affiliate_link,
            'redirect_code' => $this->redirect_code,
            'slug' => $this->slug,
            'images' => ProductImageResource::collection($this->images)
        ];
    }


}
