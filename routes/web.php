<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return redirect()->route('login');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/users', 'UserController@index')->name('admin.users');

    //categories
    Route::group(['prefix' => 'categories'], function(){
        Route::get('/',  'CategoryController@index')->name('admin.categories');
        Route::get('/delete/{id?}',  'CategoryController@destroy')->name('admin.category.delete');
        Route::post('/store',  'CategoryController@store')->name('admin.category.store');
        Route::post('/update',  'CategoryController@update')->name('admin.category.update');
    });
    Route::get('/sub-categories', 'SubCategoryController@index')->name('admin.subcategories');
    Route::get('/product-types', 'ProductTypeController@index')->name('admin.product-types');




    //subscriptions
    Route::get('/subscriptions', 'SubscriptionController@index')->name('admin.subscriptions');
    //users
    Route::group(['prefix' => 'users'], function(){
        Route::get('/', 'UserController@index')->name('admin.users');
        Route::get('/{id}', 'UserController@show')->name('admin.user.show');
        Route::get('{id}/block', 'UserController@block')->name('admin.users.block');
        Route::get('{id}/unblock', 'UserController@unblock')->name('admin.users.unblock');
        Route::get('{id}/exclude', 'UserController@destroy')->name('admin.user.delete');
        Route::get('/export/excel','UserController@excel')->name('admin.user.export.excel');
        Route::get('/export/csv','UserController@csv')->name('admin.user.export.csv');

    });

    Route::group(['prefix' => 'subscription'], function () {
        Route::get('/export/excel','SubscriptionController@excel')->name('admin.subscription.export.excel');
        Route::get('/export/csv','SubscriptionController@csv')->name('admin.subscription.export.csv');
    });


    //product management
    Route::group(['prefix' => 'products'], function(){
        Route::get('/', 'ProductController@index')->name('admin.products');
        Route::get('add', 'ProductController@create')->name('admin.product.add');
        Route::get('edit/{id}', 'ProductController@edit')->name('admin.product.edit');
        Route::get('item/{id}', 'ProductController@show')->name('admin.product.show');
        Route::post('store', 'ProductController@store')->name('admin.product.store');
        Route::post('update/{id}', 'ProductController@update')->name('admin.product.update');
        Route::get('delete/{id?}', 'ProductController@destroy')->name('admin.product.delete');
        Route::get('banner/{product}/{status}', 'ProductController@banner')->name('admin.product.banner');

    });


    //roles management
    Route::get('/roles', 'RoleController@index')->name('admin.roles');

    //permission management
    Route::get('/permissions', 'PermissionController@index')->name('admin.permisssions');

    //settings
    Route::get('/settings', 'SettingsController@index')->name('admin.settings');
    Route::get('/settings/general', 'SettingsController@general')->name('admin.general-settings');

    Route::post('/site/home', 'SettingsController@indexPage')->name('admin.site.index');
    // Route::post('/site/how-it-works', 'SettingsController@how_it_works_page')->name('admin.site.hiw');
    Route::post('/site/how-it-works', 'SettingsController@hiw_page')->name('admin.site.hiw2');

    Route::post('/site/contact', 'SettingsController@contact_page')->name('admin.site.contact');
    Route::post('/site/faq', 'SettingsController@faq_page')->name('admin.site.faq');
    Route::post('/site/instruction', 'SettingsController@instruction')->name('admin.site.instruction');
    Route::post('/site/terms', 'SettingsController@terms')->name('admin.site.terms');
    Route::post('/site/payments', 'SettingsController@payments')->name('admin.site.payments');
    Route::post('/site/guarantee', 'SettingsController@guarantee')->name('admin.site.guarantee');
    Route::post('/site/personal', 'SettingsController@personal')->name('admin.site.personal');
    Route::post('/site/upload', 'SettingsController@uploadBanner')->name('admin.site.banner');
    Route::post('/site/upload/brand', 'SettingsController@uploadBrand')->name('admin.site.brand');
    Route::post('/site/upload/company', 'SettingsController@uploadCompanyPicture')->name('admin.site.company');
    Route::get('/site/upload/delete-banner/{banner?}', 'SettingsController@deleteBanner')->name('admin.site.banner.delete');
    Route::get('/site/upload/delete-brand/{brand?}', 'SettingsController@deleteBrand')->name('admin.site.brand.delete');
    Route::get('/site/upload/delete-company/{picture?}', 'SettingsController@deleteCompanyPicture')->name('admin.site.company.delete');


});

Route::get('/home', 'HomeController@index')->name('home');
