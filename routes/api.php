<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//auth
Route::group([

//    'middleware' => 'api',
    'prefix' => 'auth'

], function () {
    //auth
    Route::post('login', 'AuthController@login');
    Route::post('register/{social?}', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('payload', 'AuthController@payload');
    Route::post('change-password','AuthController@changePassword' );
    Route::post('edit-profile','AuthController@editProfile' );
    Route::post('forgot-password','Auth\ForgotPasswordController@sendResetLinkEmail' );
    Route::post('verify','AuthController@verify' );
    Route::post('reset','Auth\ResetPasswordController@reset' );

    Route::get('resend/email', 'AuthController@resendMail');


});

Route::group(['middleware' => 'jwt','prefix' => 'user'], function () {

    Route::post('upload-avatar','UserController@uploadAvatar');


});

//page setup
Route::get('/page-setup/{page}', 'SettingsController@pageSetup');

Route::post('/contact/send-mail', function(){
    if(!empty(request('email')) && !empty(request('message'))){
        $setting = \App\Setting::where('name', 'contact')->first();
        if($setting){
            $setting = unserialize($setting->value);
            Mail::to($setting['email'])->send(new \App\Mail\ContactMail(request('email'),request('message'), request('name')));
            return apiResponse(1,trans('success'), []);
        }

    }

    return apiresponse(0, trans('errors'), request()->all());
});


//newslette
Route::post('/subscription/store', 'Api\SubscriptionController@store');

//categories
Route::group(['prefix' => 'categories','middleware' => 'jwt'], function () {
    Route::get('get-categories-not-yet-added', 'Api\CategoryController@getCategoriesNotYetAdded');
    Route::get('get-user-categories', 'Api\CategoryController@getUserCategoriesByPreferences');
});

//preference
Route::group(['prefix' => 'preferences', 'middleware' => 'jwt'], function(){
    Route::post('store', 'Api\PreferenceController@store');
    Route::get('delete/{id}', 'Api\PreferenceController@destroy');
});

//user products
Route::group(['prefix' => 'user', 'middleware' => 'jwt'], function(){
    Route::get('preferences/products', 'Api\UserController@getProductsByPreference');
    Route::get('preferences/{category}/products', 'Api\UserController@getProductsByCategory');
    Route::post('preferences/products/search', 'Api\UserController@searchPreferenceProducts');

});


//products

Route::group(['prefix' => 'product'], function(){
    Route::get('get/{id}', 'Api\ProductController@show');
    Route::get('get/similar/{id}', 'Api\ProductController@getSimilarProducts');
    Route::get('/link/get/{code}', 'Api\ProductController@getAffiliateLink')->middleware('jwt');
});


//sit statictics

Route::group(['prefix' => 'visits'], function (){
   Route::post('/get-tracker-id', 'Api\VisitController@getTrackerId');
   Route::post('/', 'Api\VisitController@store');
});

