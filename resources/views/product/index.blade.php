@extends('layouts.admin')


@section('content')

     <div class="row">
        <div class="col-12 ">
            <div class="card card-statistics">
                <div class="card-header">
                    <div class="card-heading">
                    <h4 class="card-title">{{trans('Products')}}</h4>
                    </div>
                </div>
                <div class="card-body">
                        <p class='text-right mb-10'>
                        <a  id='addCat' href="{{route('admin.product.add')}}" class="btn btn-xs btn-outline-primary" >
                                   {{trans(' Add a Product')}}
                                </a>
                                 {{-- <button type="button" class="btn btn-primary" id='categories'>
                                    Add Categories
                                </button> --}}

                        </p>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>

                                    <th scope="col" colspan="2">{{ trans('Product') }}</th>
                                    <th scope="col">{{trans('Price')}}</th>
                                    <th scope="col">{{trans('Affiliate Link')}}</th>
                                    <th scope="col">{{trans('Actions')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($products))
                                @if(count($products))
                                    @foreach ($products as $product)
                                        <tr>
                                            <th >{{ $loop->iteration }}</th>
                                            <td colspan="2">@if(isset($product->images)) <img class='img-fluid' style='width:50px;' src='{{ getImageLink($product->images[0]->path) }}'/> @else <a class="btn btn-xs" href='#'>Add Image</a> @endif{{ $product->title }}
                                            @if($product->category_id == -1)
                                            <small class="text-danger">{{trans('Please add a category.')}}</small>
                                            @endif
                                            </td>
                                            <td> {{ currency_format($product->price) }}</td>
                                            <td> {{ substr($product->affiliate_link,0,25) }} .....</td>
                                            <td>
                                            <a href="{{route('admin.product.show', $product->id)}}"  class='btn btn-xs btn-info'><i class="fa fa-eye"></i></a>
                                                <a href="{{route('admin.product.edit', $product->id)}}"  class='btn btn-xs btn-primary '><i class="fa fa-edit"></i></a>
                                                <button  data-id='{{ $product->id }}' class='btn btn-xs btn-danger delete'><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan='6'><div class='alert alert-info text-center'>{{trans('No Product added')}}</div></td>
                                    </tr>

                                @endif
                            @endif

                            </tbody>
                        </table>

                        {{$products->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>









    <div class="modal fade"  id="addCategory" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">{{trans('Add a Category')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                        <form enctype="multipart/form-data" action='{{ route('admin.category.store') }}' method='post'>
                            <div class="modal-body">
                                @csrf
                                {{-- <div class="bgc-white p-20 bd"><h6 class="c-grey-900">Add Category</h6><div class="mT-30"> --}}

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">{{trans('Name')}}</label>
                                            <input type="text" class="form-control" required value='{{ old('category') }}' name='category' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Category Name">


                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">{{trans('Description')}}</label>
                                            <textarea name='description' required class='form-control'>{{ old('description') }}</textarea>

                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">{{trans('Image')}}</label>
                                           <input type='file' class='form-control' required  name='category_image' />

                                        </div>



                            </div>
                            <div class="modal-footer">
                                {{--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  --}}
                                <button type="submit" class="btn btn-primary">Save changes</button>

                            </div>
                        </form>

                </div>
            </div>
    </div>

    <div class="modal fade" id="addMany" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">{{trans('Add Categories')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                        <form action='{{ route('admin.category.store') }}' method='post'>
                            <div class="modal-body">
                            @csrf

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">{{trans('Names')}}</label>
                                            <input type="text" class="form-control" name='category' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="{{trans('Enter category')}} ">
                                            <i class='text-muted'>{{trans('Please add categories. Separate each only with a comma')}} </i>


                                        </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>

                            </div>
                        </form>

                </div>
            </div>
        </div>


        <div class="modal fade" id="editModal" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{trans('Edit Category')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                           <form enctype="multipart/form-data" action='{{ route('admin.category.update') }}' method='post'>
                            <div class="modal-body">
                                @csrf


                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{trans('Name')}}</label>
                                                <input type="text" class="form-control" required value='{{ old('editCategory') }}' name='editCategory' id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter category email">
                                                <input type='hidden' name='id' />

                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{trans('Description')}}</label>
                                                <textarea name='description' required class='form-control'>{{ old('description') }}</textarea>

                                            </div>
                                            <div class='form-group'>
                                                <input type='checkbox' name='changeImage' id='changeImage' /> {{trans('Do you want to change/add the image?')}}
                                            </div>
                                            <div style='display:none' id='editImage' class="form-group">
                                                <label for="exampleInputEmail1">Image</label>
                                               <input type='file' class='form-control'   name='editImage' />

                                            </div>




                                </div>
                                <div class="modal-footer">
                                    {{--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  --}}
                                    <button type="submit" class="btn btn-primary">Save changes</button>

                                </div>
                            </form>

                    </div>
                </div>
            </div>


@endsection


@push('scripts')
<script>
        $(document).ready(function(){
            $('#addCat').click(function(){
                $('[name=description]').val('');
            });
            $('#categories').click(function(){
                $('[name=description]').val('');
                $('#addMany').modal();

            })
        });


        $(document).ready(function(){
            $('.edit').click(function(){
                var id = $(this).attr('data-id');
                var name = $(this).attr('data-name');
                var description = $(this).attr('data-description');
                $('[name=editCategory]').val(name);
                $('[name=description]').val(description);
                $('[name=id]').val(id);

                $('#editModal').modal();

            });
            $('.delete').click(function(){
                var answer = confirm('Are you sure?');
                var id = $(this).attr('data-id');
                if(answer){
                    window.location.href = "{{ route('admin.product.delete') }}/"+id
                }


            });


            $('#changeImage').change(function(){
                if($(this).is(':checked')){
                    $('#editImage').show();
                }else{
                    $('#editImage').hide();

                }
            });


        });

    </script>


@endpush
