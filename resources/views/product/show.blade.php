@extends('layouts.admin')
<!-- include summernote css/js -->
@push('styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet">
<style>
.carousel-item{
    /* height: auto;
    position: relative; */
    height: 300px;
    background-size: cover;

}
.carousel{
    background: #eee;
    background-blend-mode: lighten;
}
</style>
@endpush
@section('content')
    <div class="row">
            <div class="col-12 ">
                    <div class="card card-statistics">
                            <div class="card-header">
                                <div class="card-heading">
                                <h4 class="card-title">{{@$product->title}}</h4>
                                </div>
                            </div>
                            <div class="card-body">
                                    <p class='text-right mb-10'>
                                    <a  href="{{route('admin.product.edit', $product->id)}}" class="btn btn-xs btn-outline-primary" >
                                               <i class='fa fa-edit'></i>
                                            </a>
                                            <button  data-id='{{ $product->id }}' class='btn btn-xs btn-outline-danger delete'><i class="fa fa-trash-o"></i></button>
                                             {{-- <button type="button" class="btn btn-primary" id='categories'>
                                                Add Categories
                                            </button> --}}

                                    </p>
                                    <div class='row'>
                                            <div class="col-lg-6">
                                                    <div class=" card-statistics">
                                                            <div class="card-header">
                                                                <div class="card-heading">
                                                                    <h4 class="card-title">{{trans('Product Information')}}</h4>
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                <ul class="list-group">
                                                                <li class="list-group-item"><strong>{{trans('Title')}}  </strong>: {{@$product->title}}</li>
                                                                    <li class="list-group-item"><strong>{{trans('Price')}} </strong> : {{@$product->price}}</li>
                                                                    <li class="list-group-item"><strong>{{trans('Affiliate Link')}} </strong> : {{@$product->affiliate_link}}</li>
                                                                    <li class="list-group-item"><strong>{{trans('Number of views')}} </strong> : {{@$product->views()}} {{trans('views')}}</li>
                                                                    <li class="list-group-item"><strong>{{trans('Affiliate clicked')}} </strong> : {{@$product->affiliate_clicks()}} {{trans('clicks')}}</li>
                                                                    <li class="list-group-item"><strong>{{trans('Description')}} </strong>  {!! $product->description !!}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                            </div>
                                            <div class="col-lg-6">
                                                    <div class="card-statistics widget-blog-contant">
                                                        <div class="card-header">
                                                            <div class="card-heading">
                                                                <h4 class="card-title">{{trans('Product images')}}</h4>
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                            <div id="carouselExampleIndicators2" class="carousel slide pointer-event" data-ride="carousel">
                                                                <ol class="carousel-indicators">
                                                                        @for($i = 0; $i < count($product->images); $i++)
                                                                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="@if($i == 0) active @endif"></li>
                                                                        @endfor
                                                                </ol>
                                                                <div class="carousel-inner">

                                                                    @foreach($product->images as $image)
                                                                        <div style="background-image:url('{{getImageLink($image->path)}}');background-size:cover;background-position: 50% 50%;" class="carousel-item @if($loop->iteration == 1) active @endif">
                                                                          {{-- <img src="{{getImageLink($banner['path'])}}" class="d-block w-100 h-100 img-fluid" alt="..."> --}}
                                                                          {{-- <div class='overlay'>
                                                                              hhh
                                                                          </div> --}}

                                                                        </div>
                                                                    @endforeach

                                                                </div>
                                                                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                    <span class="sr-only">Previous</span>
                                                                </a>
                                                                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                    <span class="sr-only">Next</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>



                            </div>
                        </div>
            </div>
    </div>


@endsection


@push('scripts')

    <script>
     $('.delete').click(function(){
                var answer = confirm('Are you sure?');
                var id = $(this).attr('data-id');
                if(answer){
                    window.location.href = "{{ route('admin.product.delete') }}/"+id
                }


            });

    </script>

@endpush
