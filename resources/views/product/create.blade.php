@extends('layouts.admin')
<!-- include summernote css/js -->
@push('styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet">

@endpush
@section('content')



<div class="col-md-12">
        <div class="card card-statistics">
            <div class="card-header">
                <div class="card-heading">
                    <h4 class="card-title">{{trans('Add a Product')}}</h4>
                </div>
            </div>
            <div class="card-body">
            <form action="{{route('admin.product.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row mb-10">
                        <div class="col">
                        <input type="text" class="form-control" required value="{{old('title')}}" name="title" placeholder="{{('Product Title')}}">
                        </div>
                        <div class="col">
                            <input type="number" step="0.01" class="form-control" value="{{old('price')}}" required name="price" placeholder="{{('Product Price')}}">
                        </div>
                        <div class="col">
                                <input type="number" step="0.01" class="form-control" value="{{old('retail_price')}}" required name="retail_price" placeholder="{{('Product Retail Price')}}">
                        </div>
                    </div>


                    <div class="row mb-10">

                            <div class="col">
                                    <label>{{trans('Affliliate Link')}}</label>
                                <input type="url" class="form-control" value="{{old('affiliate_link')}}" required name="affiliate_link" placeholder="{{('Affiliate Link')}}">
                            </div>
                            <div class="col">
                                    <label>{{trans('Category')}}</label>
                                    <select class="form-control" required name="category_id" >
                                        <option value="">Select Category</option>
                                        @if(isset($categories))
                                            @if(count($categories))
                                                @foreach($categories as $category)

                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach

                                            @endif
                                        @endif
                                    </select>
                            </div>

                    </div>

                    <div class="row mb-10">
                            <div class="col-12">
                                    <label>{{trans('Product Description')}}</label>
                                    </div>
                            <div class="col">
                                <textarea id="summernote" required name="description">@if(!empty(old('description'))) {{old('description')}} @else {{trans('Please enter product description')}} @endif</textarea>
                            </div>

                    </div>

                    <div class="row mb-10">
                            <div class="col-12">
                            <label>{{trans('Product Images')}}</label>
                            </div>
                            <div class="col">
                                    <input required type="file" required  name="photo[]" class="dropify" />
                            </div>
                            <div class="col">
                                    <input required type="file"  required name="photo[]" class="dropify" />
                            </div>

                            <div class="col">
                                    <input required type="file" required name="photo[]" class="dropify" />
                            </div>

                            <div class="col">
                                    <input required type="file" required  name="photo[]" class="dropify" />
                            </div>


                    </div>



                    <div class="row mb-10">
                            <div class="col text-right">
                            <button type="submit" class="btn btn-primary ">{{trans('Add Product')}}</button>
                            </div>

                    </div>






                </form>
            </div>
        </div>
    </div>









@endsection


@push('scripts')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script>
        $(document).ready(function(){
            // $('#addCat').click(function(){
            //     $('[name=description]').val('');
            // });
            // $('#categories').click(function(){
            //     $('[name=description]').val('');
            //     $('#addMany').modal();

            // });
            $('#summernote').summernote({
                height: 200
            });
        });


        $(document).ready(function(){
            $('.edit').click(function(){
                var id = $(this).attr('data-id');
                var name = $(this).attr('data-name');
                var description = $(this).attr('data-description');
                $('[name=editCategory]').val(name);
                $('[name=description]').val(description);
                $('[name=id]').val(id);

                $('#editModal').modal();

            });
            $('.delete').click(function(){
                var answer = confirm('Are you sure?');
                var id = $(this).attr('data-id');
                if(answer){
                    window.location.href = "{{ route('admin.category.delete') }}/"+id
                }


            });


            $('#changeImage').change(function(){
                if($(this).is(':checked')){
                    $('#editImage').show();
                }else{
                    $('#editImage').hide();

                }
            });


        });


        $('.dropify-fr').dropify({
    messages: {
        default: 'Glissez-déposez un fichier ici ou cliquez',
        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
        remove: 'Supprimer',
        error: 'Désolé, le fichier trop volumineux'
    }
});

// Used events
var drEvent = $('.dropify').dropify();

drEvent.on('dropify.beforeClear', function(event, element) {
    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
});

drEvent.on('dropify.afterClear', function(event, element) {
    alert('File deleted');
});

drEvent.on('dropify.errors', function(event, element) {
    console.log('Has Errors');
});

var drDestroy = $('#input-file-to-destroy').dropify();
drDestroy = drDestroy.data('dropify')
$('#toggleDropify').on('click', function(e) {
    e.preventDefault();
    if (drDestroy.isDropified()) {
        drDestroy.destroy();
    } else {
        drDestroy.init();
    }
})

    </script>


@endpush
