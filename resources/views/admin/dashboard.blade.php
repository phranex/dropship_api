@extends('layouts.admin')

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css" />
<style>
    .dash-box{
        position: absolute;
    bottom: 10%;
    font-size: xx-large;
    color:blue;
    }
    .statistics{
        width: 100%
    }
    .btn-primary{
        display: none;
    }
    a{
        color: blue;
    }

</style>
@endpush

@section('content')

    <div class="row">
        <h6 class="col-12">Site's Analytics
            @if(request('from') && request('to'))
                <small>( {{request('from')}} to {{request('to')}} )</small>
            @else

            <small>(Today)</small>
            @endif
            <div class="float-right">
                <a href="{{route('admin.dashboard')}}" class="btn btn-outline-primary btn-xs" >Today</a>
                <button class="btn btn-outline-success btn-xs" data-toggle="modal" data-target="#analytics">Custom Analytics</button>

            </div>
        </h6>
        <div class="col-xs-6 col-md-4 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-pink">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{@$total_visits}}</h2>
                    <p class="text-white">Total Visits</p>
                </div>
            </div>

        </div>
        <div class="col-xs-6 col-md-4 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-success">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{@$total_new_visitors}}</h2>
                    <p class="text-white">New Visitors </p>
                </div>
            </div>

        </div>
        <div class="col-xs-6 col-md-4 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-primary">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{@$total_repeat_visitors}}</h2>
                    <p class="text-white">Repeat Visitors  </p>
                </div>
            </div>

        </div>
        <div class="col-xs-6 col-md-4 col-xxl-3 m-b-30">
            <div class="card card-statistics h-100 m-b-0 bg-orange">
                <div class="card-body">
                    <h2 class="text-white mb-0">{{@$total_logins}}</h2>
                    <p class="text-white">Total Logins</p>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
            <div class="col-sm-12">
                <div class="car card-statistics">
                    <div class="row">
                        <div class="col-xl-6 col-xxl-4 m-b-30">
                            <div class="card card-statistics h-100 mb-0">
                                <div class="card-header">
                                    <h4 class="card-title">Best selling product</h4>
                                </div>
                                <div class="card-body">
                                    @if(!empty($most_bought))
                                    <div class="blog">
                                        <img class="img-fluid" src="{{getImageLink($most_bought->images[0]->path)}}" alt="">
                                    <h4 class="m-b-10 m-t-20"><a href="{{route('admin.product.show', $most_bought->id)}}">{{$most_bought->title}}</a></h4>
                                    <p><a href="{{route('admin.product.show', $most_bought->id)}}" class="btn btn-xs btn-primary">See Product</a></p>
                                    </div>
                                    <div class="product-bar m-t-30">
                                        <div class="d-flex">
                                            <span>Orders: <b>{{$most_bought->affiliate_clicks()}}</b> </span>
                                            <span class="ml-auto">Views: <b>{{$most_bought->views()}}</b> </span>
                                        </div>
                                        {{-- <div class="progress my-3" style="height: 6px;">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div> --}}
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xxl-3 m-b-30">
                            <div class="card card-statistics h-100 mb-0 widget-income-list">
                                <div class="card-body d-flex align-itemes-center">
                                        <div class="media align-items-center w-100">
                                            <div class="text-left">
                                                <h3 class="mb-0">{{number_format($total_users)}} </h3>
                                                <span>Total Users


                                                </span>
                                                <br/>
                                                <small>Total Registration Today: {{@$total_num_of_todays_users}}</small>
                                                <br/>
                                                <small>Total Registration This Week: {{@$total_num_of_last_weeks_users}} users since <small><strong>{{date('d-m-y',$last_week)}}</strong></small></small>
                                                <br/>
                                            <small>Total Registration This Month: {{@$total_num_of_last_months_users}} users in <small><strong>{{date('M',$last_month)}}</strong></small></small>
                                            </div>
                                            <div class="img-icon bg-primary ml-auto">
                                                <i class="ti ti-tag text-white"></i>
                                            </div>
                                        </div>
                                </div>
                                <div class="card-body d-flex align-itemes-center">
                                    <div class="media align-items-center w-100">
                                        <div class="text-left">
                                            <h3 class="mb-0">{{number_format(@$total_unverified_users)}} </h3>
                                            <span>Pending Users</span>
                                        </div>
                                        <div class="img-icon bg-pink ml-auto">
                                            <i class="ti ti-user text-white"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="card-body d-flex align-itemes-center">
                                    <div class="media align-items-center w-100">
                                        <div class="text-left">
                                            <h3 class="mb-0">{{number_format(@$total_verified_users)}} </h3>
                                            <span>Active Users</span>
                                        </div>
                                        <div class="img-icon bg-info ml-auto">
                                            <i class="ti ti-slice text-white"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body d-flex align-itemes-center">
                                        <div class="media align-items-center w-100">
                                            <div class="text-left">
                                                <h3 class="mb-0">{{number_format(@$total_blocked_users)}} </h3>
                                                <span>Blocked Users</span>
                                            </div>
                                            <div class="img-icon bg-orange ml-auto">
                                                <i class="ti ti-wallet text-white"></i>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </div>
    <div class="row">
            <div class="col-sm-12">
                <div class="car card-statistics">
                    <div class="row">

                            <div class="col-md-4 m-b-30">
                                <div class="card card-statistics h-100 mb-0">
                                    <div class="card-header">
                                        <h4 class="card-title">Least selling product</h4>
                                    </div>
                                    <div class="card-body">
                                        @if(!empty($least_bought))
                                        <div class="blog">
                                            <img class="img-fluid" src="{{getImageLink($least_bought->images[0]->path)}}" alt="" />
                                            <h4 class="m-b-10 m-t-20"><a href="{{route('admin.product.show', $least_bought->id)}}">{{$least_bought->title}}</a></h4>
                                            {{-- <p>{!! substr($least_bought->description,0,70) !!} ... <a href="{{route('admin.product.show', $least_bought->id)}}" class="btn btn-xs btn-primary">See Product</a></p> --}}
                                        </div>
                                        <div class="product-bar m-t-30">
                                            <div class="d-flex">
                                                <span>Orders: <b>{{$least_bought->affiliate_clicks()}}</b> </span>
                                                <span class="ml-auto">Views: <b>{{$least_bought->views()}}</b> </span>
                                            </div>
                                            {{-- <div class="progress my-3" style="height: 6px;">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div> --}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 m-b-30">
                                    <div class="card card-statistics h-100 mb-0">
                                        <div class="card-header">
                                            <h4 class="card-title">Least Viewed</h4>
                                        </div>
                                        <div class="card-body">
                                            @if(!empty($least_viewed))
                                            <div class="blog">
                                                <img class="img-fluid" src="{{getImageLink($least_viewed->images[0]->path)}}" alt="" />
                                                <h4 class="m-b-10 m-t-20"><a href="{{route('admin.product.show', $least_viewed->id)}}">{{$least_viewed->title}}</a></h4>
                                                {{-- <p>{!! substr($least_viewed->description,0,70) !!} ... <a href="{{route('admin.product.show', $least_viewed->id)}}" class="btn btn-xs btn-primary">See Product</a></p> --}}
                                            </div>
                                            <div class="product-bar m-t-30">
                                                <div class="d-flex">
                                                    <span>Orders: <b>{{$least_viewed->affiliate_clicks()}}</b> </span>
                                                    <span class="ml-auto">Views: <b>{{$least_viewed->views()}}</b> </span>
                                                </div>
                                                {{-- <div class="progress my-3" style="height: 6px;">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div> --}}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                            </div>
                            <div class="col-md-4 m-b-30">
                                        <div class="card card-statistics h-100 mb-0">
                                            <div class="card-header">
                                                <h4 class="card-title">Most Viewed</h4>
                                            </div>
                                            <div class="card-body">
                                                @if(!empty($most_viewed))
                                                <div class="blog">
                                                    <img class="img-fluid" src="{{getImageLink($most_viewed->images[0]->path)}}" alt="">
                                                <h4 class="m-b-10 m-t-20"><a href="{{route('admin.product.show', $most_viewed->id)}}">{{$most_viewed->title}}</a></h4>
                                                {{-- <p>{!! substr($most_viewed->description,0,70) !!} ... <a href="{{route('admin.product.show', $most_viewed->id)}}" class="btn btn-xs btn-primary">See Product</a></p> --}}
                                                </div>
                                                <div class="product-bar m-t-30">
                                                    <div class="d-flex">
                                                        <span>Orders: <b>{{$most_viewed->affiliate_clicks()}}</b> </span>
                                                        <span class="ml-auto">Views: <b>{{$most_viewed->views()}}</b> </span>
                                                    </div>
                                                    {{-- <div class="progress my-3" style="height: 6px;">
                                                        <div class="progress-bar bg-success" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div> --}}
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                            </div>


                    </div>
                </div>
            </div>
    </div>
    <div class="row">
            <div class="col-xxl-12 m-b-30">
                    <div class="card card-statistics h-100 mb-0">
                        <div class="card-header">
                            <h4 class="card-title">Category/User  Report</h4>
                        </div>

                        <div class="chart-container">
                                <canvas id="chart-legend-top"></canvas>
                        </div>
                    </div>


            </div>
    </div>

    <div class="modal fade" id="analytics" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="" method="get">
                    <div class="modal-body">
                        <div class="card card-statistics mb-30">
                            <div class="card-body datepicker-form">
                                <div class="card-heading">
                                    <h4 class="card-title">Date Range</h4>
                                </div>

                                    @csrf
                                    <div class="input-group" data-date="23/11/2018" data-date-format="mm/dd/yyyy">
                                        <input data-date-end-date="0d"  autocomplete="off" required type="text" class="form-control range-from" name="from">
                                        <span class="input-group-addon">To</span>
                                        <input data-date-end-date="0d" autocomplete="off" required class="form-control range-to" type="text" name="to">
                                    </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-success">Show</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<script>

		var color = Chart.helpers.color;
		function createConfig(legendPosition, colorName) {
			return {
				type: 'line',
				data: {
					labels:[
                    @if(isset($cat_stats))
                        @if(count($cat_stats))
                            @foreach($cat_stats as $cat)
                                '{{ucwords($cat->name)}}',
                            @endforeach
                        @endif
                    @endif
                    ],
					datasets: [{
						label: 'Users',
						data: [
                            @if(isset($cat_stats))
                                @if(count($cat_stats))
                                    @foreach($cat_stats as $cat)
                                        '{{$cat->num_of_users}}',
                                    @endforeach
                                @endif
                            @endif
						],
						backgroundColor: color(window.chartColors[colorName]).alpha(0.5).rgbString(),
						borderColor: window.chartColors[colorName],
						borderWidth: 1
					}]
				},
				options: {
					responsive: true,
					legend: {
						position: legendPosition,
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Categories'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'Users'
							}
						}]
					},
					title: {
						display: true,
						text: 'Legend Position: ' + legendPosition
					}
				}
			};
		}

		window.onload = function() {
			[{
				id: 'chart-legend-top',
				legendPosition: 'top',
				color: 'red'
			}].forEach(function(details) {
				var ctx = document.getElementById(details.id).getContext('2d');
				var config = createConfig(details.legendPosition, details.color);
				new Chart(ctx, config);
			});
		};
	</script>
@endpush
