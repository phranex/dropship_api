@extends('layouts.admin')

@push('styles')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" />
<style>
    .ck-editor__editable {
        min-height: 400px;
    }

    .banner-img-holder{
        width: 200px;
        display: inline-block;
    }
    .ck.ck-toolbar{
        flex-flow: unset !important;
    }

</style>

@endpush

@section('content')

<div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body pb-0">
                <h4 class="card-title">Application Setting</h4>
                {{-- <h6 class="card-subtitle">Use default tab with class <code>customtab</code></h6> </div> --}}
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab" role="tablist">
                {{-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#others" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Instructions</span></a> </li> --}}
                <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#banner" role="tab" aria-selected="false"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Brands</span></a> </li>
                {{-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#payments" role="tab" aria-selected="false"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Automatic Payments</span></a> </li> --}}
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#terms" role="tab" aria-selected="false"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Company Picture</span></a> </li>
                {{-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#guarantee" role="tab" aria-selected="false"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">30 Day Guarantee</span></a> </li> --}}


                </ul>
                <!-- Tab panes -->
                <div class="tab-content">


                    <div class="tab-pane p-3" id="terms" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        @if(isset($company))
                                        <div class="banner-img-holder">
                                                <a  class="image-popup-no-margins btn btn-xs"  href="#"><img class="img-fluid img-thumbnail" src="{{unserialize($company->value)}}" /></a>
                                                <a class="btn btn-xs delete-banner" data-href='{{route('admin.site.company.delete',$company->id)}}' href="javascript:void"><i class="fa text-danger fa-trash-alt"></i> Delete</a>
                                        </div>

                                        @endif

                                        <div class="car card-body">
                                            <form id='tip-form'  enctype="multipart/form-data" method='post' action="{{route('admin.site.company')}}">
                                                @csrf
                                                    <div class="form-group">

                                                        <label>Company Picture</label>
                                                        <div class='banner-group row'>
                                                            <div class="col-12 col-md-4">
                                                                <div class="car">
                                                                    <div class="card-body">
                                                                            <i  class='fa fa-file remove-banner text-danger'> </i>
                                                                        {{-- <h4 class="card-title">File Upload</h4>
                                                                        <label for="input-file-now">Upload Banner</label> --}}
                                                                        <input required type="file" id="input-file-now" name="company_picture" class="dropify" />

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>




                                                            <button type="submit" class="btn btn-primary float-right submit">Submit</button>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>



                    <div class="tab-pane p-3 active" id="banner" role="tabpanel">
                        <div class="p-3">
                                <div class="row">
                                        @isset($brands)
                                            @if(count($brands))
                                            <div class="col-sm-12">
                                                <h3>Popular Brands</h3>
                                                @foreach ($brands as $item)

                                                        <div class="banner-img-holder">
                                                                <a  class="image-popup-no-margins btn btn-xs"  href="#"><img class="img-fluid img-thumbnail" src="{{$item['path']}}" /></a>
                                                                <a class="btn btn-xs delete-banner" data-href='{{route('admin.site.brand.delete',$item['id'])}}' href="javascript:void"><i class="fa text-danger fa-trash-alt"></i> Delete</a>
                                                        </div>
                                                        {{-- <small>{{@$item['content']['name']}}</small> --}}


                                                @endforeach
                                            </div>
                                            @endif

                                        @endisset
                                        <div class="col-sm-12">
                                            <div class="car card-body">
                                                <form id='tip-form'  enctype="multipart/form-data" method='post' action="{{route('admin.site.brand')}}">
                                                    @csrf
                                                        <div class="form-group">

                                                            <label>Add Brand</label>
                                                            <div class='banner-group row'>
                                                                <div class="col-12 col-md-4">
                                                                    <div class="car">
                                                                        <div class="card-body">
                                                                                <i  class='fa fa-file remove-banner text-danger'> </i>
                                                                            {{-- <h4 class="card-title">File Upload</h4>
                                                                            <label for="input-file-now">Upload Banner</label> --}}
                                                                            <input required type="file" id="input-file-now" name="brand[]" class="dropify" />

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <button type="button" class='btn add-more3 btn-sm btn-outline-primary'>Add More</button>
                                                            </div>


                                                        </div>




                                                                <button type="submit" class="btn btn-primary float-right submit">Submit</button>

                                                </form>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>




            </div>


        </div>
    </div>



@endsection

@push('scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
<script src="{{asset('assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset('/js/initialize.min.js')}}"></script>
<script src="{{asset('assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>



<script>
        var editor = ClassicEditor.create( document.querySelector( '#terms_' ) ).then( editor => {
                            @isset($contents['terms']['editordata'])
                                editor.setData('{!! $contents['terms']['editordata'] !!}');
                            @endisset
                            console.log( editor );
                            }).catch( error => {
                                    console.error( error );
                                });


        var editor2 = ClassicEditor
                        .create( document.querySelector( '#personal_' ) )
                        .then( editor2 => {
                            @isset($contents['privacy']['editordata'])
                                editor2.setData('{!! $contents['privacy']['editordata'] !!}');
                            @endisset
                            console.log( editor2 );
                        } ).catch( error2 => {
                            console.error( error2 );
                            } );


        var editor3 = ClassicEditor
                        .create( document.querySelector( '#guarantee_' ) )
                        .then( editor3 => {
                                @isset($contents['guarantee']['editordata'])
                                    editor3.setData('{!! $contents['guarantee']['editordata'] !!}');
                                @endisset
                        console.log( editor3 );
                        } ).catch( error3 => {
                        console.error( error3 );
                        } );


        var editor4 = ClassicEditor
                        .create( document.querySelector( '#payments_' ) )
                        .then( editor4 => {
                                @isset($contents['payments']['editordata'])
                                    editor4.setData('{!! $contents['payments']['editordata'] !!}');
                                @endisset
                                console.log( editor4 );
                        } ).catch( error4 => {
                            console.error( error4 );
                        } );


    </script>
    <Script>
         no_of_banner = 1;
        value = 1000;
            $('.learn-more').click(function(){
                $('#learn-more').modal();
            });

            $('.delete-banner').click(function(){
               var url = $(this).attr('data-href');
                var answer = confirm('Are you sure? Click OK to continue');
                if(answer){
                    window.location.href = url;
                }
            });

            $('.add-faq').click(function(){

                var html = `<div id='remove${value}'><label>Question</label> <button type='button' class='btn btn-xs btn-danger remove-faq' target='#remove${value}' ><i class='fa fa-trash'></i></button>
                                <textarea required name='question${value}' class="form-control" rows="2"></textarea>
                            <label>Answer</label>
                                <textarea required name='answer${value}' class="form-control" rows="5"></textarea></div>`;
                value++;
                $('.add-more').append(html);
            });

            $('.add-step').click(function(){

var html = `<div id='remove${value}' style='margin-top:10px'><label>Question</label> <button type='button' class='btn btn-xs btn-danger remove-step' target='#remove${value}' ><i class='fa fa-trash'></i></button>
                <textarea required name='step${value}' class="form-control" rows="2"></textarea>
            <label>Answer</label>
                <textarea required name='answer${value}' class="form-control" rows="5"></textarea></div>`;
value++;
$('.add-more2').append(html);
});

            $(document).on('click', '.remove-faq', function(){
                var target = $(this).attr('target');
                $(target).remove();
            });

            $(document).on('click', '.remove-step', function(){
                var target = $(this).attr('target');
                $(target).remove();
            });


            $(document).ready(function() {
                        // Basic
        $('.dropify').dropify();


// Translated
$('.dropify-fr').dropify({
    messages: {
        default: 'Glissez-déposez un fichier ici ou cliquez',
        replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
        remove: 'Supprimer',
        error: 'Désolé, le fichier trop volumineux'
    }
});

// Used events
var drEvent = $('#input-file-events').dropify();

drEvent.on('dropify.beforeClear', function(event, element) {
    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
});

drEvent.on('dropify.afterClear', function(event, element) {
    alert('File deleted');
});

drEvent.on('dropify.errors', function(event, element) {
    console.log('Has Errors');
});

var drDestroy = $('#input-file-to-destroy').dropify();
drDestroy = drDestroy.data('dropify')
$('#toggleDropify').on('click', function(e) {
    e.preventDefault();
    if (drDestroy.isDropified()) {
        drDestroy.destroy();
    } else {
        drDestroy.init();
    }
})


});

// $('.add-more3').click(function(){

//     no_of_banner++;
//     var html = ` <div class="col-12 col-md-4">
//                                                                     <div class="car">
//                                                                         <div class="card-body">

//                                                                             <input required type="file" id="input-file-now" name="photo[]" class="dropify" />
//                                                                             <label style="margin-top:10px">Review</label>
//                                                                             <input required class="form-control"  type="text" name='caption' />
//                                                                             <label style="margin-top:10px">Name</label>
//                                                                             <input required class="form-control"  type="text" name='caption-small' />
//                                                                         </div>
//                                                                     </div>
//                                                                 </div>`;
//     $('.banner-group').append(html);
//     $.initialize(".dropify", function() {
//                     $(this).dropify();
//                 });

//                 $('.remove-banner').click(function(){
//                     $(this).parents('div.banner-item').remove();


//                 });
// });


$('.add-more3').click(function(){

no_of_banner++;
var html = ` <div class="col-12 banner-item col-md-4">
                                                                    <div class="car">
                                                                        <div class="card-body">
                                                                            <i style='cursor:pointer'  class='fa fa-trash remove-banner text-danger'> Delete</i>
                                                                    <input required type="file"  name="brand[]" class="dropify" />



                                                                </div>
                                                            </div>
                                                        </div>`;
$('.banner-group').append(html);
$.initialize(".dropify", function() {
                $(this).dropify();
            });

            $('.remove-banner').click(function(){
                $(this).parents('div.banner-item').remove();


            });
});
    </script>
@endpush
