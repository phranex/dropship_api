@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12 ">
            <div class="card card-statistics">
                    <div class="card-header">
                            <div class="card-heading">
                                <h4 class="card-title">Subscriptions</h4>
                            <a style='margin:2px' class="btn btn-xs btn-outline-primary float-right" href="{{route('admin.subscription.export.excel')}}">Export to Excel</a>
                            <a style='margin:2px' class="btn btn-xs btn-outline-primary float-right" href="{{route('admin.subscription.export.csv')}}">Export to CSV</a>

                            </div>
                    </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">email</th>

                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($subscriptions))
                                @if(count($subscriptions))
                                    @foreach ($subscriptions as $subscription)
                                        <tr>
                                            <th >{{ $loop->iteration }}</th>
                                            <td>{{$subscription->email}}</td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan='4'><div class='alert text-center'>{{trans('No Subscription added')}}</div></td>
                                    </tr>

                                @endif
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
