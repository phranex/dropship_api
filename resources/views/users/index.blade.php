@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-12 ">
            <div class="card card-statistics">
                <div class="card-header">
                    <div class="card-heading">
                        <h4 class="card-title">Users</h4>
                    <a style='margin:2px' class="btn btn-xs btn-outline-primary float-right" href="{{route('admin.user.export.excel')}}">Export to Excel</a>
                    <a style='margin:2px' class="btn btn-xs btn-outline-primary float-right" href="{{route('admin.user.export.csv')}}">Export to CSV</a>

                </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">{{trans('Name')}}</th>
                                    <th scope="col">{{trans('Email')}}</th>
                                    <th scope="col">{{trans('Verified')}}</th>
                                    <th scope="col">{{trans('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($users)
                                    @if(count($users))
                                        @foreach ($users as $user)
                                            <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$user->first_name}} {{$user->last_name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>
                                                    @if($user->verified)
                                                        <i class="fa fa-check-circle text-success"></i> {{trans('Verified')}}
                                                    @else
                                                        <i class="fa fa-times-circle text-danger"></i> {{trans('Not Verified')}}
                                                    @endif
                                                </td>
                                                <td>
                                                <a href="{{route('admin.user.show', $user->id)}}" class="btn btn-xs btn-primary"><i class=" fa fa-eye"></i></a>
                                                @if($user->blocked)
                                                <a href='{{route('admin.users.unblock',$user->id)}}' class='btn btn-xs btn-outline-warning'> <i class="fa fa-unlock"></i> {{trans('Unblock Access')}}</a>
                                                @else
                                                    <a href='{{route('admin.users.block',$user->id)}}' class='btn btn-xs btn-outline-danger'><i class="fa fa-lock"></i> {{trans('Block Access')}}</a>
                                                @endif
                                                    <a class='btn btn-xs btn-danger delete-user' href="{{route('admin.user.delete',$user->id)}}"><i class="fa fa-trash-o "></i>  {{trans('Exclude')}}</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                    @endif
                                @endisset

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
    <script>
        $(document).ready(function(){
            $('.delete-user').click(function(){
                event.preventDefault();
                var answer = confirm('Are you sure you want to exclude this user? Click Ok to continue otherwise cancel');
                if(answer){
                    window.location.href = $(this).attr('href');
                }
            });
        });
    </script>

@endpush
