@extends('layouts.admin')
@push('styles')
    <style>


.xyz{
    border: 1px solid #bbb;

    padding: 0.5vh;margin:4px 5px;
    display:inline-block
}

.xyz:hover{
    border:2px solid #0897FF;
    font-weight: bold;
    cursor: pointer;
}


    </style>

@endpush

@section('content')
<div class="row account-contant">
        <div class="col-12">
            <div class="card card-statistics">
                <div class="card-body p-0">
                    <div class="row no-gutters">
                        <div class="col-xl-3 pb-xl-0 pb-5 border-right">
                            <div class="page-account-profil pt-5">
                                <div class="profile-img text-center rounded-circle">
                                    <div class="pt-5">

                                        <div class="profile pt-4">
                                        <h4 class="mb-1">{{$user->first_name}} {{$user->last_name}}</h4>
                                        <p><i class="fa fa-envelope"></i> <a href="mailto:{{$user->email}}">{{$user->email}}</a></p>
                                        <small>{{trans('Registered')}} {{when($user->created_at)}}</small>
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="py-5 profile-counter">
                                    <ul class="nav justify-content-center text-center">
                                        <li class="nav-item border-right px-3">
                                            <div>
                                                <h4 class="mb-0">90</h4>
                                                <p>Post</p>
                                            </div>
                                        </li>

                                        <li class="nav-item border-right px-3">
                                            <div>
                                                <h4 class="mb-0">1.5K</h4>
                                                <p>Messages</p>
                                            </div>
                                        </li>

                                        <li class="nav-item px-3">
                                            <div>
                                                <h4 class="mb-0">4.4K</h4>
                                                <p>Members</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div> --}}

                                <div class="profile-btn text-center">
                                    {{-- <div><button class="btn btn-light text-primary mb-2">Upload New Avatar</button></div> --}}
                                    <div>
                                            @if($user->blocked)
                                            <a href='{{route('admin.users.unblock',$user->id)}}' class='btn btn-xs btn-outline-warning'>{{trans('Unblock Access')}}</a>
                                            @else
                                                <a href='{{route('admin.users.block',$user->id)}}' class='btn btn-xs btn-outline-danger'>{{trans('Block Access')}}</a>
                                            @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-md-8 col-12 ">
                            <div class=" tabs-contant">
                                    <div class="col-12">
                                        <div class=" card-statistics">

                                            <div class="card-body">
                                                <div class="tab round">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li class="nav-item">
                                                        <a class="nav-link show active" id="home-07-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="home-07" aria-selected="true">  <span class="d-none d-md-block">Profile</span></a>
                                                        </li>
                                                        <li class="nav-item">
                                                            {{-- <a class="nav-link show" id="profile-07-tab" data-toggle="tab" href="#password" role="tab" aria-controls="profile-07" aria-selected="false"><span class="d-none d-md-block">Change Password</span> </a> --}}
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link show" id="portfolio-07-tab" data-toggle="tab" href="#preferences" role="tab" aria-controls="portfolio-07" aria-selected="false"> <span class="d-none d-md-block">Preferences</span> </a>
                                                        </li>

                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane fade py-3 active show" id="profile" role="tabpanel" aria-labelledby="home-07-tab">
                                                                {{-- <div class="card-body">
                                                                        <div class="media">
                                                                            <img class="mr-3 mb-3 mb-xxs-0 img-fluid" src="http://localhost:1000/dashboard/assets/img/avtar/01.jpg" alt="image">
                                                                            <div class="media-body">
                                                                                <h5 class="mt-0">Savvyone</h5>
                                                                                Since there is not an "all the above" category, I'll take the opportunity to enthusiastically congratulate you on the very high quality, "user-friendly" documentation.
                                                                            </div>
                                                                        </div>
                                                                    </div> --}}

                                                                    <form action="#" class='form' method='post'  >
                                                                        @csrf
                                                                        <div class="form-group">
                                                                            <label class="control-label">{{ __('First Name') }}*</label>
                                                                        <input readonly type="text" name="first_name" value="@if(!empty(old('first_name'))) {{ old('first_name') }}@else{{$user->first_name}}@endif" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First Name" name="name" value="{{ old('first_name') }}" required autofocus />
                                                                            @if ($errors->has('first_name'))
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label">{{ __('Last Name') }}*</label>
                                                                            <input readonly type="text" name="last_name" value="@if(!empty(old('last_name'))) {{ old('last_name') }}@else{{$user->last_name}}@endif" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="Last Name" />
                                                                            @if ($errors->has('last_name'))
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('last_name') }}</strong>
                                                                            </span>
                                                                        @endif
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="control-label">{{ __('Email') }}*</label>
                                                                            <input type="email" readonly name="email" value="{{ $user->email }}" class="form-control"  />

                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label">{{ __('Phone Number') }}</label>
                                                                            <input type="number" readonly name="phone_number" value="{{ $user->phone_number }}" class="form-control" placeholder="Phone Number" />

                                                                        </div>


                                                                        {{-- <div class="text-right">
                                                                                <button type="submit" class="btn btn-primary">{{trans('Update')}}</button>
                                                                        </div> --}}


                                                                    </form>



                                                        </div>
                                                        {{-- <div class="tab-pane fade py-3" id="password" role="tabpanel" aria-labelledby="profile-07-tab">
                                                            <form method="post"  action='#' class="form-horizontal form-material">
                                                                @csrf
                                                                    <div class="form-group">
                                                                    <label class="col-md-12">{{trans('Old Password')}}</label>
                                                                        <div class="col-md-12">
                                                                        <input type="password" name ='old_password'  class="form-control form-control-line">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="example-email" class="col-md-12">{{trans('New Password')}}</label>
                                                                        <div class="col-md-12">
                                                                            <input type="password" name='password' class="form-control form-control-line" name="example-email" id="example-email">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-12">{{trans('Confirm Password')}}</label>
                                                                        <div class="col-md-12">
                                                                            <input type="password" value="" name='password_confirmation' class="form-control form-control-line">
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button class="btn btn-success">{{trans('Change Password')}}</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                        </div> --}}
                                                        <div class="tab-pane fade py-3" id="preferences" role="tabpanel" aria-labelledby="portfolio-07-tab">
                                                            <div class="ro" >
                                                            {{-- <a href="{{route('user.preference.create')}}" class="btn btn-link pull-right btn-sm" style="margn:20px;font-size:12px !important">{{trans('Add Preferences')}}</a> --}}
                                                            {{-- @if(count($user->preferences)) <div style="clear:both"><small>Click to remove</small></div> @endif --}}
                                                                @isset($user->preferences)
                                                                    {{-- @if(count($user->preferences))
                                                                        <div  class="category-box-mobile owl-carousel categories w-100">
                                                                            @foreach($user->preferences as $category)
                                                                            <div class="category-holder">
                                                                                <div class='category-item'>
                                                                                <button data-id='{{$category['id']}}'>
                                                                                        <img style="width:50px;height:50px" src="{{$category['category_image']}}" class="img-fluid" />
                                                                                        <span>{{$category['name']}}</span>
                                                                                </button>

                                                                                </div>

                                                                            </div>
                                                                            @endforeach



                                                                        </div>
                                                                    @endif --}}

                                                                    @if(count($user->preferences))
                                                                        <div   class="category-box  categories-m w-100">
                                                                            @foreach($user->preferences->chunk(5) as $categorys)
                                                                            <div  class="category-holde">
                                                                                    @foreach($categorys as $category)
                                                                                <div title="Click to delete" style="" class='xyz'>
                                                                               <a href="javascript:void" class="pref" data-id='{{$category->category->id}}'>
                                                                                <img style="width:50px;height:50px" src="{{getImageLink($category->category->path)}}" class="img-fluid" />
                                                                                <span>{{$category->category->name}}</span>
                                                                               </a>

                                                                                </div>
                                                                                @endforeach


                                                                            </div>
                                                                            <div style="clear:both;margin-top:8px"></div>
                                                                            @endforeach
                                                                              {{-- <div class='category-item-md'>
                                                                                    <a href="{{route('user.dashboard')}}">
                                                                                            <img style="width:50px" src="{{$category['path']}}" class="img-fluid" />
                                                                                            <span>{{trans('All')}}</span>
                                                                                    </a>

                                                                                    </div> --}}


                                                                        </div>
                                                                    @endif
                                                                @endisset
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                    </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
