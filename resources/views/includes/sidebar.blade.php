<aside class="app-navbar">
    <!-- begin sidebar-nav -->
    <div class="sidebar-nav scrollbar scroll_light">
        <ul class="metismenu " id="sidebarNav">
            <li class="nav-static-title">Personal</li>
            <li class="@if(request()->route()->getName() ==  'admin.dashboard') active @endif">
            <a class="" href="{{route('admin.dashboard')}}" aria-expanded="false">
                    <i class="nav-icon ti ti-rocket"></i>
                    <span class="nav-title">Dashboard</span>

                </a>

            </li>
        <li class="@if(request()->route()->getName() ==  'admin.users') active @endif"><a href="{{route('admin.users')}}" aria-expanded="false"><i class="nav-icon ti ti-comment"></i><span class="nav-title">Users Management</span></a> </li>

            <li class="@if(request()->route()->getName() ==  'admin.categories') active @endif"><a class="has-arro" href="{{route('admin.categories')}}" aria-expanded="true"><i class="nav-icon ti ti-calendar"></i><span class="nav-title">Categories Management</span></a>
                {{-- <ul aria-expanded="false" class="collapse" style="">
                <li> <a href="">Categories </a> </li>
                    <li> <a href="{{route('admin.subcategories')}}">SubCategories</a> </li>
                    <li> <a href="{{route('admin.product-types')}}">Product Type</a> </li>
                </ul> --}}
            </li>
        <li class="@if(request()->route()->getName() ==  'admin.products') active @endif"><a href="{{route('admin.products')}}" aria-expanded="false"><i class="nav-icon ti ti-email"></i><span class="nav-title">Products</span></a> </li>
        {{-- <li class="@if(request()->route()->getName() ==  'admin.roles') active @endif"><a href="{{route('admin.roles')}}" aria-expanded="false"><i class="nav-icon ti ti-comment"></i><span class="nav-title">Roles</span></a> </li> --}}
        {{-- <li class="@if(request()->route()->getName() ==  'admin.permisssions') active @endif"><a href="{{route('admin.permisssions')}}" aria-expanded="false"><i class="nav-icon ti ti-comment"></i><span class="nav-title">Permissions</span></a> </li> --}}
        <li class="@if(request()->route()->getName() ==  'admin.subscriptions') active @endif"><a href="{{route('admin.subscriptions')}}" aria-expanded="false"><i class="nav-icon ti ti-comment"></i><span class="nav-title">Subscriptions</span></a> </li>

            <li class="@if(request()->route()->getName() ==  'admin.settings') active @endif">
            <a class="" href="{{route('admin.settings')}}" aria-expanded="false"><i class="nav-icon ti ti-bag"></i> <span class="nav-title">Settings</span></a>

            </li>

            <li class="@if(request()->route()->getName() ==  'admin.general-settings') active @endif">
                    <a class="" href="{{route('admin.general-settings')}}" aria-expanded="false"><i class="nav-icon ti ti-bag"></i> <span class="nav-title">General Settings</span></a>

                    </li>
            {{-- <li class="@if(request()->route()->getName() ==  'user.dashboard-profile') active @endif">
                <a class="has-arrow" href="" aria-expanded="false"><i class="nav-icon ti ti-info"></i><span class="nav-title">Profile</span> </a>

            </li> --}}
            {{--<li class="nav-static-title">Widgets, Tables & Layouts</li>--}}

            {{--<li class="@if(request()->route()->getName() ==  'user.reviews') active @endif">--}}
                {{--<a class="has-arrow" href="{{route('user.reviews', 'user')}}" aria-expanded="false"><i class="nav-icon ti ti-layout-column3-alt"></i><span class="nav-title">Settings</span></a>--}}

            {{--</li>--}}

            {{--<li class="@if(request()->route()->getName() ==  'user.reviews') active @endif">--}}
                {{--<a class="has-arrow" href="{{route('user.reviews', 'user')}}" aria-expanded="false"><i class="nav-icon ti ti-layout-column3-alt"></i><span class="nav-title">Support</span></a>--}}

            {{--</li>--}}





        </ul>
    </div>
    <!-- end sidebar-nav -->
</aside>
